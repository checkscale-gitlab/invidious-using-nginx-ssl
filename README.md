## **Invidious with Nginx SSL**

It is an example on how to implement invidious with nginx where HTTP is automatically redirected to HTTPS.

### **Nginx configuration**

<table>
<tr>
<th>
In conf.d
</th>
</tr>

<tr>
<td>

Rename `your-domain.conf` with your domain name.

In the file, change `subdomain.your-domain.com` with your domain or subdomain plus domain name.

</td>
</tr>
</table>

<table>
<tr>
<th>
In ssl
</th>
</tr>

<tr>
<td>

Create a folder named `subdomain.your-domain.com` and inside it you need to put ***fullchain.pem*** and ***privkey.pem***.

You can use [certbot](https://certbot.eff.org/) to generate ***fullchain.pem*** and ***privkey.pem***.

</td>
</tr>
</table>

### **Docker-compose configuration**

<table>
<tr>
<th>
In docker-compose.yml
</th>
</tr>

<tr>
<td>

Change `your_password` with custom strong password.

</td>
</tr>
</table>

### **Run Invidious**

The first time you need to build *nginx* and *postgres*:
```sh
docker-compose up --build -d
```

Once build, run it with this command:
```sh
docker-compose up -d
```

### **More Informations**

You can find `postgres/entrypoint` script on the [official Invidious Github](https://github.com/iv-org/invidious/tree/master/docker).

You can find `postgres/sql` files on the [official Invidious Github](https://github.com/iv-org/invidious/tree/master/config/sql).

### **Documentation**

[Invidious Github](https://github.com/iv-org/invidious)

[Invidious Documentation](https://docs.invidious.io/en/Installation)